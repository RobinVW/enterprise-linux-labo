## Cheat sheet en checklists

- Naam student: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

Vul dit document zelf aan met commando's die je nuttig vindt, of waarvan je merkt dat je het verschillende keren moeten opzoeken hebt. Idem voor checklists voor troubleshooting, m.a.w. procedures die je kan volgen om bepaalde problemen te identificeren en op te lossen. Voeg waar je wil secties toe om de structuur van het document overzichtelijk te houden. Werk dit bij gedurende heel het semester!

Je vindt hier alvast een voorbeeldje om je op weg te zetten. Zie [https://github.com/bertvv/cheat-sheets](https://github.com/bertvv/cheat-sheets) voor een verder uitgewerkt voorbeeld.

### Commando's

| Taak                   | Commando |
| :---                   | :---     |
| IP-adress(en) opvragen | `ip a`   |


### Git workflow

Eenvoudige workflow voor een éénmansproject

| Taak                                        | Commando                  |
| :---                                        | :---                      |
| Huidige toestand project                    | `git status`              |
| Bestanden toevoegen/klaarzetten voor commit | `git add FILE...`         |
| Bestanden "committen"                       | `git commit -m 'MESSAGE'` |
| Synchroniseren naar Bitbucket               | `git push`                |
| Wijzigingen van Bitbucket halen             | `git pull`                |

### Checklist netwerkconfiguratie

1. Is het IP-adres correct? `ip a`
2. Is de router/default gateway correct ingesteld? `ip r -n`
3. Is er een DNS-server? `cat /etc/resolv.conf`


### Vagrant commando's
- **Vagrant box add [name]/[version]** : invoegen van de vagrant box (matyunin/centos7)
- **Vagrant up **: launchen van de VM : daarna kan je deze launchen in VBox
- **Vagrant halt** : afzetten
- **Vagrant reload** : rebooten
- **Vagrant provision** : ansible playbooks herladen op de machine.
- **Vagrant destroy -F** : verwijderen van de VM(s)
- **Vagrant box list** : lijst van alle VMs in de box.

### Services controlleren/starten/stoppen
- **sudo systemctl [start/stop/restart] [naam-service].service **: de genoemde service starten/stoppen/herstarten.
- **sudo systemctl status [naam-service].service** : de status van de service opvragen.
- **sudo systemctl [enable/disable] [naam-service].service** : de servers wel/niet opstarten bij het booten.


### SELinux Booleans en Context
- **getsebool –a**: toont alle booleans die SELinux bevat en hun status.
- **setsebool -P [naamBoolean] [0/1]**: Booleans aan- en uitschakelen
- **setenforce [0/1]: uit- en aanschakelen van SELinux
- **sudo chcon  [-r/-t/-u] [samba_share_t] [alpha] : de context veranderen.


### FTP commando's
- **curl ftp://172.16.0.11/directie/ --user USERNAME:PASSWORD**: ftp testen


### Bind commando's
- **dig @[ip-of-dnsServer] [FQDN] [NS/A]**: het IP-adres opvragen van de server die geregistreerd staat met deze FQDN. De laatste parameter is optioneel en vraagt de specifieke records op.
- **dig @[ip-of-dnsServer] –x [ip-adres] **: de Fully Qualified Domain Name opvragen van de server met het gegeven ip adres


### Firewall commando's
- **Firewall-cmd --status** : status van de firewall
- **Firewall-cmd –list-all-zones** : overzicht van alle services en poorten die worden gefilterd.
- **Firewall-cmd [--permanent] [--zone=ZONE] --add-service=http** : de service toevoegen aan de firewall
- **Firewall-cmd [--permanent] [--zone=ZONE] --add-port=80/tcp** : poort 80/tcp openzetten.


### Configuraties testen
- **named-checkconf [locatie conf]**: configuratie testen _named.conf_
- **named-checkzone [naam-zone-file] [locatie-zone-file]*: configuratie van _zone-file_ testen.
- **testparm** : controleert configuratie van _Samba config file_.


### Toestemmingen
- Numeriek: chmod 775 [bestandsnaam/path]: toegangsrechten voor een bestand/dir regelen, - R zorgt voor recursie in een dir.
- Symbolisch: chmod [u/g/o/a][+/=/-][r/w/x/t] [bestandsnaam] : toegangsrechten voor een bestand/dir regelen, - R zorgt voor recursie in een dir.


### Samba configuratie 
_[staf]_ --> naam van de share
  _comment = staf_ --> een commentaarregel
  _path = /srv/shares//staf_ --> het pad naar de map waar de share zich bevind
  _public = no_ --> is deze map publiek? Yes/no
  _write list = franka,femkevdv,maaiked,robin_ --> personen of groepen (+/@) die mogen schrijven
  _force group = staf_ --> groep waarmee de share gedeelt is
  _create mask = 0775_ --> write list, standaard mask voor aanmaken file
  _force create mode = 0775_ --> forge group, standaar mask voor aanmaken file
  _directory mask = 0775_ --> mask van de directories die aangemaakt worden binnen share
  _force directory mode = 0775_ --> mask van de directories die aangemaakt worden binnen share (voor force group)


  

### Locaties van config files en andere belangrijke bestanden

| Locatie	| Uitleg |
|:---		| :---     |
| **/etc/named.conf**	| Bind config file |
| **/var/named/[zone name]	**| Bind zone file |
|** /etc/vsftpd/vsftpd.conf	**| VSFTP config file |
| **/etc/samba/smb.conf**	| Samba config file |
| **/srv/shares **	|	Samba shares locatie (ls -l)	|
| **/etc/pki/tls/certs**	| SSL certificaat locatie   |
| **/etc/pki/tls/private/ca.key**	| SSL locatie private key   |
| **/etc/httpd/conf.d/ssl.conf	**| SSL config file       |
| **/etc/httpd/conf/httpd.conf	**| http config file        |
| **/etc/dhcp/dhcpd.conf	**| DHCP config file   |
| **/etc/httpd/conf.d/wordpress.conf**	| Secundaire configs (vn wordpress bv)  |
| **/etc/sysconfig/network-scripts	**| In deze map staan de config files voor alle netwerkinterfaces  |
| **/var/log/...**	| Oude logfiles mappen     |





























