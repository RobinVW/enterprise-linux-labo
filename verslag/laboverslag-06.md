## Laboverslag: 06-DHCP

- Naam cursist: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

### Procedures

1. Host var pr001.yml aanmaken
2. Op de 2de machine zoeken naar het mac-adres van 1 adapter (ifconfig -a)
3. machine in de hostvar een adres toewijzen

### Testplan en -rapport

- kijken of het toegewezen ip adres overeenkomt:

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Alles

#### Wat ging niet goed?

Mijn nieuwe werkomgeving had enkele errors.

#### Wat heb je geleerd?

Een DHCP server configureren (basisinstellingen, subnets maken en die adressenranges geven, 
adresreservaties adhv MAC adres instellen)

#### Waar heb je nog problemen mee?

Mijn virtualbox op mijn laptop

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
