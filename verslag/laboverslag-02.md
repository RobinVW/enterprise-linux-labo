## Laboverslag: taak 02 lamp-stack

- Naam cursist: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

### Procedures

1. de Pu004.yml gemaakt waarin alle onderdelen van de LAMP stack automatisch geinstalleerd wordt
2. de firewall ingesteld
3. het certificaat installeren

### Testplan en -rapport

- Alle code zit in de bitbucket repository
- De VM met apache + PHP eb mariaDB werkt
- De firewall- en SELinux-instellingen zijn correct
- de pagina op 192.0.2.50 werkte in de klas

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- De onderdelen van de LAMP stack installeren
- De firewall instellingen gingen ook, maar minder vlot

#### Wat ging niet goed?

- Het certificaat

#### Wat heb je geleerd?

Een LAMP stack maken met MariaDB

#### Waar heb je nog problemen mee?

Het self-signed certificaat

### Referenties

https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/ch-Web_Servers.html