## Laboverslag: 05: webserver

- Naam cursist: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

### Procedures

  
FTP Vsftpd
\\serverip\share

Samba 
1. Host var 'pr011.yml' opstellen
2. in de test file de admin naam in robinvw veranderd
3. password hashes toegevoegd aan maaiked en robinvw
4. fouten opgelost in de yml 
5. nieuwe test schrijven

(FTP Vsftpd
\\serverip\share)

VSFTP
6. vsftp geinstalleerd van weareinteractive (openssl verwijderd)
7. anonieme toegang weigeren
8. fouten opgelost zodat het lezen en schrijven van de users op de juiste shares klopte
9. SEBooleans aanpassen zodat de laatste falende test slaagde

### Testplan en -rapport

![testplan samba & vsftp](https://i.gyazo.com/e5884206e3777094fbfc99f96732dbd6.png)

- connecteren via filezilla met ftp lukt

### Retrospectieve


#### Wat ging goed?

- De sambaserver opstellen ging redelijk vlot, ookal waren eer weer enkele foutjes in de yaml file.

#### Wat ging niet goed?

De lees en schrijfrechten,
ik moest eerst de nodige SEBooleans aanzetten

#### Wat heb je geleerd?

- Sambaserver opzetten en configureren met ansible + eventuele problemen oplossen
- Vsftp-server opzetten en configureren met ansible + eventuele problemen oplossen
- SELinux booleans bekijken en aanpassen

#### Waar heb je nog problemen mee?

/

### Referenties

Hulp van klasgenoten en leerkracht