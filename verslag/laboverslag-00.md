## Laboverslag: TITEL

- Naam cursist: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

### Procedures

Ik heb:
1. een account op bitbucket gemaakt
2. Git geconfigureerd op mijn pc
3. SSH key gemaakt
4. een repository gemaakt op bitbucket
5. mijn repository upgedate

### Testplan en -rapport

- vagrant status werkte
- vagrant up werkte
- inloggen op de server werkte
- login zonder wachtwoord werkte

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- alles ging goed, maar niet alles ging even vlotjes

#### Wat ging niet goed?

* enkele fouten gemaakt in all.yml


#### Wat heb je geleerd?

* Met git leren werken
* Met Ansible leren werken
* Met Vagrant leren werken

#### Waar heb je nog problemen mee?

En zijn er evt. nog vragen die open blijven en die je aan de lector wil voorleggen?

### Referenties

https://help.github.com/articles/generating-ssh-keys/