## Laboverslag: TAAK 03

- Naam cursist: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

### Procedures

1. nieuwe hosts toegevoegd aan de vagrant_hosts.yml
2. pu001.yml en pu002.yml aangemaakt
3. BIND geconfigureerd in beide bestanden 
4. firewall geconfigureerd (in host vars)

### Testplan en -rapport

- Forward lookups slagen voor publieke en private hosts               test geslaagd
- Reverse lookups slagen voor publieke hosts                          test geslaagd
- De DNS-server antwoordt op de DNS-requests vanop het hostsysteem    test geslaagd
- Alias lookups slagen                                                test geslaagd
- NS & MX records slagen                                              test geslaagd
- Reverse lookups slagen voor de private hosts                        test geslaagd  
- Alle tests op de slave server slagen                                test geslaagd  

### Retrospectieve



#### Wat ging goed?

- de DNS servers aanmaken
- de host vars aanmaken

#### Wat ging niet goed?

De testen op de slave server
--> opgelost door een vagrant destroy en opnieuw vagrant up te doen

#### Wat heb je geleerd?

- De onderdelen van een DNS server
  (config files van BIND, zone files)
- DNS server testen 
  
#### Waar heb je nog problemen mee?

/

### Referenties

/
