## Laboverslag: 04 troubleshooting

- Naam cursist: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

### Procedures

1. toetsenbord van qwerty naar azerty veranderd
2. interface enp0s8 opgezet
3. fouten in named.config oplossen
4. fouten in de 192.168.56.in-addr.arpa file opgelost
5. fouten in cynalco.com (/var/named/cynalco.com) opgelost
6. Named.service gestart


### Testplan en -rapport

- Forward lookups van de private servers: 	test geslaagd
- Forward lookups van de public servers: 	test geslaagd
- Reverse lookups van de private servers: 	test gefaald
- Reverse lookups van de public servers: 	test geslaagd
- Alias lookups van de private servers 		test gefaald
- Alias lookups van de public servers: 		test geslaagd
- NS record lookup: 						test gefaald
- Mail server lookup: 						test geslaagd

### Retrospectieve



#### Wat ging goed?

- de lookups voor de public servers

#### Wat ging niet goed?

Een syntaxfout in de named.config file zorgde voor veel problemen.
Ook om te beginnen met deze opdracht heb ik veel gesukkeld.

#### Wat heb je geleerd?

Ik heb veel meer bijgeleerd over de onderdelen van een dns server.

#### Waar heb je nog problemen mee?

de private lookups

### Referenties

/