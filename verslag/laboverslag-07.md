## Laboverslag: Troubleshooting 2: SAMBA SERVER

- Naam cursist: Robin Van Wezemael
- Bitbucket repo: https://bitbucket.org/RobinVW/enterprise-linux-labo

### Procedures

1. Keyboard naar azerty veranderen (loadkeys be-latin1)
2. Lees- en schrijfrechten goed gezet in de smb.conf file

 
### Testplan en -rapport

- Alle tests falen

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- enkele fouten oplossen in de config file

#### Wat ging niet goed?

- Alles

#### Wat heb je geleerd?

/

#### Waar heb je nog problemen mee?

de shares die "er niet waren", de netbios resolution, enkele fouten in de config die ik niet vond
en virtualbox die geen machines meer wil booten

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
